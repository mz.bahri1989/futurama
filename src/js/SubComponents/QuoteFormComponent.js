import React from "react";
import {connect} from "react-redux";
import {submitFormAction} from "../Store/actions/submitFormAction";
import {updateInputAction} from "../Store/actions/updateInputAction";
import {updateCharacterAction} from "../Store/actions/updateCharacterAction";
import {showErrorAction} from "../Store/actions/showErrorAction";

class QuoteFormComponent extends React.Component {
    constructor(props) {
        super(props);
        this.searchQuote = this.searchQuote.bind(this);
        this.saveInput = this.saveInput.bind(this);
        this.selectCharacter = this.selectCharacter.bind(this);
    }

    searchQuote(e) {
        e.preventDefault();
        const {character, keyPhrase} = this.props.form;
        const{notTouched} = this.props.results;
        if(notTouched){
            character.length > 0 ? this.props.submitFormAction({
                    character: character.toLowerCase().replace(/\s/g, '-'),
                    keyPhrase
                }): this.props.showErrorAction()
        }

    }

    saveInput(e) {
        this.props.updateInputAction(e.target.value)
    }
    selectCharacter(e) {
        this.props.updateCharacterAction(e.target.value)

    }
    render() {
        return (
            <form method="post" onSubmit={this.searchQuote}>
                <fieldset>
                    <input onChange={this.selectCharacter}
                                 list="characters"
                                 type="text"
                           className={this.props.form.noCharacterSelected ? 'err':''}
                                 placeholder="Type a character name ... "/>
                    <datalist id="characters">
                        {
                            this.props.characters.map((item, index) => {
                                return <option key={index}>{item}</option>
                            })
                        }
                    </datalist>
                </fieldset>
                <fieldset>
                    <input
                    type="text"
                    placeholder="Look for quote ..."
                    onInput={this.saveInput}
                />
                    <input type="submit" value="go"/>
                </fieldset>
            </form>
        )
    }
}

const mapStateToProps = ({form,characters,results}) => {
    return {
        form,
        characters,
        results
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        submitFormAction: (payload) => dispatch(submitFormAction(payload)),
        updateInputAction: (payload) => dispatch(updateInputAction(payload)),
        updateCharacterAction: (payload) => dispatch(updateCharacterAction(payload)),
        showErrorAction: ()=> dispatch(showErrorAction())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuoteFormComponent);