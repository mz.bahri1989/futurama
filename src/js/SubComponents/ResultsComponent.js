import React, {Fragment} from "react";
import {connect} from "react-redux";

class ResultsComponent extends React.Component {
    constructor(props) {
        super(props);
        this.renderQuotesList = this.renderQuotesList.bind(this);
        this.refineResults = this.refineResults.bind(this);
    }

    refineResults() {
        const {results, form} = this.props;
        return results.raw.filter(item => item.quote.toLowerCase().indexOf(form.keyPhrase.toLowerCase()) > -1)
    }

    renderQuotesList() {
        const {character, keyPhrase} = this.props.form;
        const refined = this.refineResults();
        if (keyPhrase.length > 0) {
            return refined.length > 0 ? <Fragment>
                <ul>
                    {
                        refined.map((item, index) => {
                            return <li key={index}>{item.quote}</li>
                        })
                    }
                </ul>
            </Fragment> : <p>{`Sorry! "${character}" has no quotes about "${keyPhrase}".`}</p>
        }
        return <ul>
            {
                this.props.results.raw.map((item, index) => {
                    return <li key={index}>{item.quote}</li>
                })
            }
        </ul>
    }

    render() {
        const {results} = this.props;
        if (results.notTouched) {
            return '';
        }
        return this.renderQuotesList()
    }
}

const mapStateToProps = ({results, form}) => {
    return {
        results,
        form

    }
};
const mapDispatchToProps = (dispatch) => {
    return {}
};
export default connect(mapStateToProps, mapDispatchToProps)(ResultsComponent);