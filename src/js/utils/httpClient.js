export function httpClient(requestProps = {}){
    const{method,url,data,headers} = requestProps;
    return new Promise((resolve, reject)=>{
        if(!url || url===''){
           return reject('no url!')
        }
        const xhr = new XMLHttpRequest();
        if(headers){
            const keys = Object.keys(headers);
            keys.map((key)=>{
                xhr.setRequestHeader(key,headers[key])
            })
        }
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
               return resolve(JSON.parse(xhr.responseText))
            }
            else if(xhr.readyState === 4 && xhr.status !== 200){
                return reject(xhr)
            }
        };
        xhr.open(method, url, true);
        data ? xhr.send(data): xhr.send();
    })
}