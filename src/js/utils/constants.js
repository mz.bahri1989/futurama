export const _SEARCH_QUOTES_URL = 'http://futuramaapi.herokuapp.com/api/quotes?search=';
export const _CHARACTERS_URL = 'http://futuramaapi.herokuapp.com/api/v2/characters';
export const _CHARACTERS_QUOTES_URL = 'http://futuramaapi.herokuapp.com/api/characters/';
export const _FIND_CHARACTER_URL = 'http://futuramaapi.herokuapp.com/api/v2/characters?search=';