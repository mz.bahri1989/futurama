import React from "react";
import ResultsComponent from "../SubComponents/ResultsComponent";
import {connect} from "react-redux";
import QuoteFormComponent from "../SubComponents/QuoteFormComponent";

import "../../css/index.scss";

class HomeComponent extends React.Component {
    constructor(props) {
        super(props);
    }



    render() {
        return (
            <main>
                <h1>FUTURAMA</h1>
                <p>Select Your Favorite Character to Read Their Quotes</p>
               <QuoteFormComponent />
                {this.props.results.raw.length > 0 ? <ResultsComponent/>:''}
            </main>
        )
    }
}

const mapStateToProps = ({results,characters}) => {
    return {
        characters,
        results
    }
};
const mapDispatchToProps = (dispatch) => {
    return {

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);