import {_DATA_RECEIVED, _GET_DATA} from "../types";
import {put, call, takeLatest} from 'redux-saga/effects';
import {httpClient} from "../../utils/httpClient";
import {_CHARACTERS_QUOTES_URL} from "../../utils/constants";

function* getData({payload}) {
    console.log(payload);
    try {
        const data = yield call(httpClient,{
            method: 'GET',
            url: _CHARACTERS_QUOTES_URL +payload.character
        });
        yield put({
            type: _DATA_RECEIVED,
            payload: {
                raw: data
            }
        })
    } catch (err) {
        console.log(err);
    }

}

export function* getDataWatcher() {
    yield takeLatest(_GET_DATA, getData)
}