import {_DATA_RECEIVED, _UPDATE_CHARACTER_FIELD} from "../types";
const _INITIAL = {
    raw:[],
    notTouched: true
};
export function ResultsReducer(state=_INITIAL, action){
    switch (action.type) {
        case _DATA_RECEIVED:
            return {
                ...state,
                ...action.payload,
                notTouched: false
            };
        case _UPDATE_CHARACTER_FIELD:
            return {
                ...state,
                raw:[],
                notTouched:true
            };
        default:
            return state;
    }
}