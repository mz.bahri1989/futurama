import {_CHARACTER_ERR, _UPDATE_CHARACTER_FIELD, _UPDATE_SEARCH_FIELD} from "../types";

const _INITIAL = {
    keyPhrase: '',
    character: '',
    noCharacterSelected: false
}
export function FormReducer(state=_INITIAL,action){
    switch(action.type){
        case _UPDATE_SEARCH_FIELD:
            return {
                ...state,
                keyPhrase: action.payload
            };
        case _CHARACTER_ERR:
            return {
                ...state,
                noCharacterSelected: true
            };
        case _UPDATE_CHARACTER_FIELD:
            return {
                ...state,
                noCharacterSelected: false,
                character: action.payload
            };
        default:
            return state;
    }
}