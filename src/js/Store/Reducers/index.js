import {combineReducers} from "redux";
import {ResultsReducer} from "./ResultsReducer";
import {FormReducer} from "./FormReducer";
import {CharactersReducer} from "./CharactersReducer";

const rootReducer = combineReducers({
    results: ResultsReducer,
    form: FormReducer,
    characters: CharactersReducer
})
export default rootReducer