import {_ALL_CHARACTERS_RECEIVED} from "../types";
const _INITIAL = [
    "Bender",
    "Fry",
    "Leela",
    "Professor Farnsworth",
    "Amy",
    "Zapp Brannigan",
    "Lurr",
    "Dr Zoidberg",
    "Linda the reporter",
    "Bob Barker",
    "Hermes",
    "Morgan Proctor",
    "Mom",
    "Robot Mob",
    "Giant Bender",
    "Kif",
    "Don bot",
];
export function CharactersReducer(state=_INITIAL, action){
    switch(action.type){
        case _ALL_CHARACTERS_RECEIVED:
            return [
                ...state,
                ...action.payload
            ];

        default:
            return state;
    }
}