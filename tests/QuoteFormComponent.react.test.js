import React from 'react';
import renderer from 'react-test-renderer';
import QuoteFormComponent from "../src/js/SubComponents/QuoteFormComponent";
import store from "../src/js/Store";
import {Provider} from "react-redux";

test('QuoteForm component loads successfully', () => {
    const component = renderer.create(
        <Provider store={store}>
            <QuoteFormComponent />
        </Provider>
        ,
    );
    let quoteForm = component.toJSON();
    expect(quoteForm).toMatchSnapshot();
});