import {httpClient} from "../src/js/utils/httpClient";
import {_CHARACTERS_QUOTES_URL} from "../src/js/utils/constants";

test('The app connects to API', () => {
    return httpClient({method:'get',url:_CHARACTERS_QUOTES_URL+'kif'}).then(data => {
        expect(data).toBe('It\'s not uncharted, you lost the chart!');
    });
});